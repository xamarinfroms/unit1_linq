﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit1_LinQ
{
    class Program
    {
        public static event EventHandler<int> OnTest;
        static void Main(string[] args)
        {


            List<int> result = null;
             int[] scores = new int[] { 2, 3, 10, 20, 50, 100 };
            result = scores.OrderByDescending(c => c).ToList();
            
            Console.WriteLine(string.Join( ",", result.ToArray()));

            Console.WriteLine(result.FirstOrDefault());
            Console.WriteLine(result.SingleOrDefault(c => c == 2));
            Console.WriteLine(result.Count());
            Console.WriteLine(result.Sum());
            Console.WriteLine(result.Max());
            Console.WriteLine(result.Min());

            Console.ReadKey();












           // List<int> result = new List<int>();


            foreach (var i in scores)
            {
                if (i > 20)//查找陣列中值大於20
                {
                    result.Add(i);
                }
            }
            Console.WriteLine(string.Join(",", result.ToArray()));
            Console.ReadKey();


            result = scores.Where(x => x > 20).ToList();

            scores.Select(c => c.ToString());

          




            //    = new List<int>();


            result =
                (from source in scores
                 where source > 20
                 select source).ToList();


            scores.Select((c, index) =>new { Index=index,Value=c } );

            Console.ReadKey();

            Program.OnTest += Test;

            Program.OnTest += (sender, e) =>
            {
                Console.WriteLine(e);
            };

            Program.OnTest+=(sender,e)=> Console.WriteLine(e);


            Program.OnTest += (sender, e) => Console.WriteLine(e);

        }

        private static void Test(object sender,int e)
        {
            Program.OnTest += Program_OnTest;
        }

        private static void Program_OnTest(object sender, int e)
        {
            throw new NotImplementedException();
        }
    }


}
